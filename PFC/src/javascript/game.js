const question = document.getElementById("question");
const choices = Array.from(document.getElementsByClassName("choice-text"));
const progressText = document.getElementById("progressText");
const scoreText = document.getElementById("score");
const progressBarFull = document.getElementById("progressBarFull");
let currentQuestion = {};
let acceptinAnswers = true;
let score = 0;
let questionCounter = 0;
let availableQuestions = [];
let questions = []
fetch("../json/questions.json")
    .then(res => {
        return res.json();
    })
    .then(loaderQuestions => {
        console.log(loaderQuestions);
        questions = loaderQuestions;
        startGame();
    })
    .catch(err => {
        console.error(err);
    });
//constantes
const CORRECT_BONUS = 10;
const MAX_QUESTIONS = 10;
var answerShow = [];
startGame = () => {
    questionCounter = 0;
    score = 0;
    availableQuestions = [...questions];
    getNewQuestion();
};
getNewQuestion = () => {
    if(availableQuestions.length == 0 || questionCounter >= MAX_QUESTIONS){
        localStorage.setItem("mostRecentScore", score);
        return window.location.assign("../pages/feedback.html");
    }
    questionCounter++;
    progressText.innerText = `Pergunta ${questionCounter}/${MAX_QUESTIONS}`;
    progressBarFull.style.width = `${(questionCounter / MAX_QUESTIONS) * 100}%`;
    const questionIndex = Math.floor(Math.random() * availableQuestions.length);
    currentQuestion = availableQuestions[questionIndex];
    question.innerText = currentQuestion.question;
    choices.forEach( choice => {
        const number = choice.dataset['number'];
        choice.innerText = currentQuestion['choice' + number];
    });
    availableQuestions.splice(questionIndex, 1);
    acceptinAnswers = true;
}; 
choices.forEach(choice => {
    choice.addEventListener("click", e => {
        if(!acceptinAnswers) return;
        acceptinAnswers = false;
        const selectedChoice = e.target;
        const selectedAnswer = selectedChoice.dataset["number"];
        const classToApply = selectedAnswer == currentQuestion.answer ? 'correct' : 'incorrect';
        if (classToApply == 'correct') {
            incrementScore(CORRECT_BONUS);
        }
        selectedChoice.parentElement.classList.add(classToApply); 
        var next = document.getElementById("next");
        next.onclick = () => {
            if(acceptinAnswers == true){
                window.alert("Escolha uma alternativa.");
                return;
            }
            selectedChoice.parentElement.classList.remove(classToApply);
            getNewQuestion();
        };
        var answerText;
        switch (selectedAnswer){
            case '1':
                answerText = 'Favorece';
                break;
            case '2':
                answerText = 'Neutro';
                break;
            case '3':
                answerText = 'Prejudica';
                break;
        }
        var answerText3;
        switch (classToApply){
            case 'correct':
                answerText3 = 'CERTA';
                break;
            case 'incorrect':
                answerText3 = 'ERRADA';
                break;
        }
        answerShow.push({
            qustion: currentQuestion.question,
            chosenAnswer: answerText,
            //answer: answerText2,
            correctAnswer: answerText3,
        });
        console.log(answerShow);
        localStorage.setItem('answerShow', JSON.stringify(answerShow));
    });
});
incrementScore = num => {
    score += num;
    scoreText.innerText = score;
};
